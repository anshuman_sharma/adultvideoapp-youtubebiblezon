package com.biblezone.youtubeplay;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import com.squareup.picasso.Picasso;
import com.youtubeplay.youtube.OpenYouTubePlayerActivity;

public class Dummy extends Activity {

	ListView lv;
	String[] URL = {"https://www.youtube.com/watch?v=W7YNa0JYqro&list=PLA6CDEC04243E8643&index=1",
			"https://www.youtube.com/watch?v=i7wDHHFuXpY&index=2&list=PLA6CDEC04243E8643",
			"https://www.youtube.com/watch?v=zSc8eyL9n8c&list=PLA6CDEC04243E8643&index=3",
			"https://www.youtube.com/watch?v=qs6Gln2ocb0&index=4&list=PLA6CDEC04243E8643",
			"https://www.youtube.com/watch?v=ERQFY2OE3_Y&index=5&list=PLA6CDEC04243E8643",
			"https://www.youtube.com/watch?v=1K5kt2PTLMg&list=PLA6CDEC04243E8643&index=6",
			"https://www.youtube.com/watch?v=WYXMJmZ1tFY&index=7&list=PLA6CDEC04243E8643",
			"https://www.youtube.com/watch?v=kYaaQWuD2YM&index=8&list=PLA6CDEC04243E8643",
			"https://www.youtube.com/watch?v=vJOl6ttzxpA&list=PLA6CDEC04243E8643&index=9",
			"https://www.youtube.com/watch?v=C0WfMwAL2RY&list=PLA6CDEC04243E8643&index=10",
	"https://www.youtube.com/watch?v=nlwC5z8j0do&index=11&list=PLA6CDEC04243E8643"};

	String[] title = {"Saint For Kids",
			"The Story Of Saint Perpetua - Arrest Scene",
			"The Story Of Saint Perpetua - Trial Scene",
			"The Story Of Saint Perpetua - Dream Scene",
			"The Story Of Saint Perpetua - Journaling Scene",
			"The Story Of Saint Perpetua - Martyrdom Scene",
			"Saint Therese (Preview)",
			"The Story of Blessed Imelda",
			"Mary, mother of Jesus",
			"LADY OF THE ROSES (JUAN DIEGO MESSENGER OF GUADALUPE)CLIP",
	"St. John Bosco (animated bio)"};

	String[] by = {"paulinesav","catholicHeroes","catholicHeroes","catholicHeroes",
			"catholicHeroes","catholicHeroes","Brother Francis Online.com",
			"Brother Francis Online.com","Brother Francis Online.com","majestic1976","gravasco"};

	CustomAdapter adapter;

	TextView tv_title;
	EditText et_srch;
	ImageView img_srh;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lv = (ListView) findViewById(R.id.listView1);
		tv_title = (TextView) findViewById(R.id.textView1);
		Typeface mfont = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/romy.ttf");
		tv_title.setTypeface(mfont);
		et_srch = (EditText) findViewById(R.id.et_1);
		img_srh = (ImageView) findViewById(R.id.imageView2);
		//ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.list_item, URL);
		adapter = new CustomAdapter(URL, getImgArray(URL), title, by);
		lv.setAdapter(adapter);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				/*Intent i = new Intent(MainActivity.this, PlayActivity.class);
				i.putExtra("url", URL[arg2]);
				startActivity(i);*/

				String videoId = getYoutubeVideoId(URL[arg2]).toString();

				if (videoId == null || videoId.trim().equals("")) {
					return;
				}

				Intent lVideoIntent = new Intent(null, Uri.parse("ytv://" + videoId), Dummy.this, OpenYouTubePlayerActivity.class);
				startActivity(lVideoIntent);
			}
		});

		et_srch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				if(adapter != null)
				{
					adapter.getFilter().filter(s.toString());
				}
				/*else  && s.length()>0
				{
					pur_Adapter.notifyDataSetInvalidated();
				}*/
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		
		img_srh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(et_srch.getVisibility()==(View.VISIBLE))
				{
					et_srch.setText("");
					et_srch.setVisibility(View.GONE);
				}
				else
				{
					et_srch.setVisibility(View.VISIBLE);
				}
				
			}
		});
	}

	class CustomAdapter extends BaseAdapter implements Filterable{
		String[] title_array;
		String[] img_array;
		String[] name;
		String[] by;

		String[] title_array_1;

		public CustomAdapter(String[] title_array, String[] img_array, String[] name, String[] by) {
			this.title_array = title_array;
			title_array_1 = title_array;
			this.img_array = img_array;
			this.name = name;
			this.by = by;
		}

		@Override
		public int getCount() {
			return title_array.length;
		}

		@Override
		public Object getItem(int arg0) {
			return title_array[arg0];
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			if(arg1 == null)
			{
				Log.e("asdasd", "in gteview");
				LayoutInflater inflater = (LayoutInflater) Dummy.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				arg1 = inflater.inflate(R.layout.list_item, arg2, false);
			}

			ImageView iv = (ImageView) arg1.findViewById(R.id.thumb_image);
			TextView tv = (TextView) arg1.findViewById(R.id.title);
			Typeface mfont1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/mvboli.ttf");
			tv.setTypeface(mfont1);
			tv.setText(title[arg0]);
			TextView tv_1 = (TextView) arg1.findViewById(R.id.by);
			tv_1.setTypeface(mfont1);
			tv_1.setText("by "+by[arg0]);
			Picasso.with(Dummy.this).load(img_array[arg0]).into(iv);

			return arg1;
		}
		@Override
		public Filter getFilter() {
			return  new VideoFilter();
		}

		private class VideoFilter extends Filter {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				constraint = constraint.toString().toLowerCase();
				FilterResults results = new FilterResults();

				if (constraint != null && constraint.length() > 0) {
					String[] tit = null;
					int k = 0;
					//Log.e("filter 1", "" + raw_data);
					for (int i = 0; i < title_array_1.length; i++) {
						Log.e("filter 2", "" + title_array_1);
						String vieo_name = title_array_1[i].toString().toLowerCase();

						if (vieo_name.contains(constraint)) {
							tit[k] = vieo_name;
							k++;
						}
					}
					results.count = tit.length;
					results.values = tit;
				} 
				else {
					results.count = title_array_1.length;
					results.values = title_array_1;
					Log.e("filter 4", "" + results);
					Log.e("filter 44", "" + title_array_1);
				}

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				if (constraint != null && constraint.length() > 0) {

					title_array = (String[]) results.values;
					Log.e("filter 5", "" + title_array);
					notifyDataSetChanged();

				} else {
					title_array = title_array_1;
					Log.e("filter 6", "" + title_array);
					notifyDataSetChanged();
				}
			}
		}
	}

	public static String getYoutubeVideoId(String youtubeUrl) {
		String video_id = "";
		if (youtubeUrl != null && youtubeUrl.trim().length() > 0
				&& youtubeUrl.startsWith("http")) {

			String expression = "^.*((youtu.be"
					+ "\\/)"
					+ "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; 
			CharSequence input = youtubeUrl;
			Pattern pattern = Pattern.compile(expression,
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(input);
			if (matcher.matches()) {
				String groupIndex1 = matcher.group(7);
				if (groupIndex1 != null && groupIndex1.length() == 11)
					video_id = groupIndex1;
			}
		}
		return video_id;
	}

	public String[] getImgArray(String[] url){
		String[] img_arr = new String[url.length];
		for(int i = 0; i<url.length; i++){
			img_arr[i] = "http://img.youtube.com/vi/"+getYoutubeVideoId(url[i])+"/0.jpg";
			Log.e("asdasd", "http://img.youtube.com/vi/"+getYoutubeVideoId(url[i])+"/0.jpg");
		}
		return img_arr;
	}
}