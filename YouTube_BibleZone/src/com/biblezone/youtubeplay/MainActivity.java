package com.biblezone.youtubeplay;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.biblezone.Rest.CheckLatestVersionAsynctask;
import com.biblezone.Rest.RestClient;
import com.biblezone.Rest.RestClient.RequestMethod;
import com.youtubeplay.youtube.OpenYouTubePlayerActivity;

public class MainActivity extends Activity {

	ArrayList<String> url_List = new ArrayList<String>();
	ArrayList<String> title_List = new ArrayList<String>();
	ArrayList<String> by_List = new ArrayList<String>();
	
	ListView lv;
	
	VideoAdapter adapter;

	TextView tv_title;
	EditText et_srch;
	ImageView img_srh;
	video_list_task vlt;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lv = (ListView) findViewById(R.id.listView1);
		tv_title = (TextView) findViewById(R.id.textView1);
		Typeface mfont = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/romy.ttf");
		tv_title.setTypeface(mfont);
		et_srch = (EditText) findViewById(R.id.et_1);
		img_srh = (ImageView) findViewById(R.id.imageView2);
		
		if(checkNewtworkConnection(MainActivity.this))
		{
			vlt = new video_list_task();

			if (Build.VERSION.SDK_INT >= 11) {

				vlt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
			} 
			else 
			{
				vlt.execute("");
			}
			new CheckLatestVersionAsynctask(this).execute();
		}
		else
		{
			Toast.makeText(MainActivity.this, "You are not connected to internet.", Toast.LENGTH_LONG).show();
		}

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
			
				String videoId = getYoutubeVideoId(url_List.get(arg2)).toString();

				if (videoId == null || videoId.trim().equals("")) {
					return;
				}

				Intent lVideoIntent = new Intent(null, Uri.parse("ytv://" + videoId), MainActivity.this, OpenYouTubePlayerActivity.class);
				startActivity(lVideoIntent);
			}
		});

		et_srch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				if(adapter != null)
				{
					adapter.getFilter().filter(s.toString());
				}
				/*else  && s.length()>0
				{
					pur_Adapter.notifyDataSetInvalidated();
				}*/
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		
		img_srh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(et_srch.getVisibility()==(View.VISIBLE))
				{
					et_srch.setText("");
					et_srch.setVisibility(View.GONE);
				}
				else
				{
					et_srch.setVisibility(View.VISIBLE);
				}
				
			}
		});
	}
	

	public static String getYoutubeVideoId(String youtubeUrl) {
		String video_id = "";
		if (youtubeUrl != null && youtubeUrl.trim().length() > 0
				&& youtubeUrl.startsWith("http")) {

			String expression = "^.*((youtu.be"
					+ "\\/)"
					+ "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; 
			CharSequence input = youtubeUrl;
			Pattern pattern = Pattern.compile(expression,
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(input);
			if (matcher.matches()) {
				String groupIndex1 = matcher.group(7);
				if (groupIndex1 != null && groupIndex1.length() == 11)
					video_id = groupIndex1;
			}
		}
		return video_id;
	}

	public ArrayList<String> getImgArray(ArrayList<String> url){
		ArrayList<String> img_arr = new ArrayList<String>();
		for(int i = 0; i<url.size(); i++){
			img_arr.add("http://img.youtube.com/vi/"+getYoutubeVideoId(url.get(i))+"/0.jpg");
			Log.e("asdasd", "http://img.youtube.com/vi/"+getYoutubeVideoId(url.get(i))+"/0.jpg");
		}
		return img_arr;
	}
	
	class video_list_task extends AsyncTask<String, Integer, Integer> {
		ProgressDialog pd;
		JSONObject json;
		String statusMsg = "";
		String statusCode = "";
		String total_pages = "";
		String total_records = "";

		@Override
		protected Integer doInBackground(String... param) {

			String response = "";
			//String final_url = "http://202.157.76.19/biblezon/webservices/videolist";
			String final_url = "http://biblezon.com/appapicms/webservices/videolist";
			Log.d("Final url", final_url.replaceAll(" ","%20"));
			RestClient client = new RestClient(final_url.trim().toString().replaceAll(" ","%20"));

			try {

				client.Execute(RequestMethod.GET);
				response = client.getResponse();
				json = new JSONObject(response);
				Log.d("jObject", ""+json);
				statusCode = json.getString("replyCode");
				statusMsg = json.getString("replyMsg").toString();
				
				if(statusCode.equalsIgnoreCase("success"))
				{
					Log.d("statusCode in back", statusCode);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			pd.dismiss();
			Log.d("statusCode in post", statusCode);
			
			if(statusCode.equalsIgnoreCase("success"))
			{
				if(json.has("data"))
				{
					 
					try {
						JSONArray j_array = json.getJSONArray("data");
						Log.d("j_array", ""+j_array);
						
						for(int j =0; j<j_array.length();j++)
						{
							JSONObject video_object = j_array.getJSONObject(j);
							Log.e("video_array", ""+video_object);
							
							String id = video_object.getString("id");
							url_List.add(video_object.getString("url").toString());
							title_List.add(video_object.getString("title").toString());
							by_List.add(video_object.getString("by").toString());
							
							Log.e("id", video_object.getString("id"));
							Log.e("title", video_object.getString("title"));
							Log.e("url", video_object.getString("url"));
							Log.e("by", video_object.getString("by"));
						}
						adapter = new VideoAdapter(MainActivity.this, url_List, getImgArray(url_List), title_List, by_List);
						lv.setAdapter(adapter);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					Log.d("Json ", "not have user data");
				}
			}
			else
			{
				//Toast.makeText(BarList.this, statusMsg, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			pd = ProgressDialog.show(MainActivity.this, "", "Loading ...");
		
		}
	}
	
	public static boolean checkNewtworkConnection(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if ((mWifi != null && mWifi.isConnected() && mWifi.isAvailable())
				|| (netInfo != null && netInfo.isConnected() && netInfo
				.isAvailable()))
			return true;
		else
			return false;
	}


	//----------------------- auto update code start ----------------------------------------------//

	DownloadManager downloadManager;
	private long downloadReference;

	// broadcast receiver to get notification about ongoing downloads
	private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			// check if the broadcast message is for our Enqueued download
			long referenceId = intent.getLongExtra(
					DownloadManager.EXTRA_DOWNLOAD_ID, -1);
//            AndroidAppUtils.showLog(TAG,
//                    "downloadReference *******  referenceId : "
//                            + downloadReference + " ******** " + referenceId);
			if (downloadReference == referenceId) {

//                AndroidAppUtils.showVerboseLog("VersionCheckAPIHandler",
//                        "Downloading of the new app version complete");
				// start the installation of the latest version
				Intent installIntent = new Intent(Intent.ACTION_VIEW);
				installIntent.setDataAndType(downloadManager
								.getUriForDownloadedFile(downloadReference),
						"application/vnd.android.package-archive");
				installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(installIntent);

			}
		}
	};

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		IntentFilter filter = new IntentFilter(
				DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		registerReceiver(downloadReceiver, filter);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			if (downloadReceiver != null) {
				unregisterReceiver(downloadReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//----------------------- auto update code end ------------------------------------------------//
}