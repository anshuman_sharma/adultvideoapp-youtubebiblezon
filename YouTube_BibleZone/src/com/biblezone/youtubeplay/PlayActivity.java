package com.biblezone.youtubeplay;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.os.Bundle;
import android.widget.Toast;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;


public class PlayActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

	//public static final String DEVELOPER_KEY = "AIzaSyB392fCyty70WA5C67WIrYky05Nm2WgNq4";
	public static final String DEVELOPER_KEY = "AIzaSyAJ9H_xwkJKEdzhdTZPDgaH8PDikzkF70Q";

	//public static final String YOUTUBE_VIDEO_CODE = "_oEA18Y8gM0";
	public static String YOUTUBE_VIDEO_CODE = "";

	private YouTubePlayerView youTubeView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
		
		YOUTUBE_VIDEO_CODE = getYoutubeVideoId(getIntent().getExtras().getString("url"));
		
		youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
		
		youTubeView.initialize(DEVELOPER_KEY, this);
	}

	@Override
	public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
		Toast.makeText(this, ""+arg1.toString(), Toast.LENGTH_LONG).show();

	}

	@Override
	public void onInitializationSuccess(Provider arg0, YouTubePlayer player, boolean arg2) {
		if (!arg2) {
			player.loadVideo(YOUTUBE_VIDEO_CODE);
			player.setShowFullscreenButton(false);
		}

	}
	
	public static String getYoutubeVideoId(String youtubeUrl) {
		String video_id = "";
		if (youtubeUrl != null && youtubeUrl.trim().length() > 0
				&& youtubeUrl.startsWith("http")) {

			String expression = "^.*((youtu.be"
					+ "\\/)"
					+ "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; 
			CharSequence input = youtubeUrl;
			Pattern pattern = Pattern.compile(expression,
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(input);
			if (matcher.matches()) {
				String groupIndex1 = matcher.group(7);
				if (groupIndex1 != null && groupIndex1.length() == 11)
					video_id = groupIndex1;
			}
		}
		return video_id;
	}
}