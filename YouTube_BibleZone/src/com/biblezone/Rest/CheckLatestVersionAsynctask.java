package com.biblezone.Rest;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;


import com.biblezone.youtubeplay.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Karan on 5/1/2016.
 */
public class CheckLatestVersionAsynctask extends AsyncTask<Void, Void, Void> {

    Context context;
    ProgressDialog pDialog;
    JSONObject json;

    DownloadManager downloadManager;
    private long downloadReference;
    public static final String FETCH_LATEST_VERSION = "http://biblezonadmin.com/biblezon/apk/checkversion.php?app=%s&deviceId=%s";

    public CheckLatestVersionAsynctask(Context context){
        this.context = context;
        pDialog = ProgressDialog.show(context,null,"Loading, please wait..");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        String response = "";
        String url = String.format(FETCH_LATEST_VERSION, context.getPackageName(), Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        Log.d("Final url", url.replaceAll(" ", "%20"));
        RestClient client = new RestClient(url.trim().toString().replaceAll(" ", "%20"));

        try {
            //client.AddParam("video_category_id", id);
            client.Execute(RestClient.RequestMethod.GET);
            response = client.getResponse();
            json = new JSONObject(response);

            Log.d("jObject", "" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (pDialog != null && pDialog.isShowing()){
            pDialog.dismiss();
        }
        try {
            if (json != null && json.getBoolean("success")) {
                onAutoUpdateAPiResponse(json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void onAutoUpdateAPiResponse(JSONObject response) {
			/* Success of API Response */
        try {
            int latest_version = Integer.valueOf(response.getString("latestVersion"));
            final String appURI = response.getString("appURI");
            PackageInfo pInfo = null;
            try {
                pInfo = context.getPackageManager().getPackageInfo(
                        context.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            int versionCode = pInfo.versionCode;

            if (latest_version > versionCode) {
                // oh yeah we do need an upgrade, let the user know send
                // an alert message
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        context);
                builder.setMessage(
                        "There is newer version of this application available, click OK to upgrade now?")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    // if the user agrees to upgrade
                                    public void onClick(
                                            DialogInterface dialog, int id) {
                                        // start downloading the file
                                        // using the download manager
                                        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                                        Uri Download_Uri = Uri
                                                .parse(appURI);
//                                            AndroidAppUtils.showLog(TAG,
//                                                    "appURI : " + appURI);
                                        DownloadManager.Request request = new DownloadManager.Request(
                                                Download_Uri);
                                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                                        request.setAllowedOverRoaming(false);
                                        request.setTitle(context.getResources().getString(
                                                R.string.app_name));
                                        request.setDestinationInExternalFilesDir(
                                                context,
                                                Environment.DIRECTORY_DOWNLOADS,
                                                context.getPackageName()
                                                        + ".apk");
                                        downloadReference = downloadManager
                                                .enqueue(request);
                                    }
                                });
                // show the alert message
                builder.create().show();
            } else {
                // mResponseListener.onSuccessOfResponse();
            }

        } catch (Exception e) {
            e.printStackTrace();
            // mResponseListener.onSuccessOfResponse();
        }


    }
}
